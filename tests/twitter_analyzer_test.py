import unittest
from lib import tweets_analyzer
import json
import os

SAMPLE_TIMELINE = os.path.join(os.path.dirname(__file__), 'sample.json')

class Tests(unittest.TestCase):
    def test_clean_tweet(self):
        tweet = 'Hello this a test tweet, mentionning @someuser, to check this http://www.google.com'
        clean = tweets_analyzer.clean_tweet(tweet)

        self.assertEquals(clean, "Hello this a test tweet, mentionning  to check this ")

    def test_get_language_from_tweet(self):
        tweet = 'Hello this a test tweet, mentionning @someuser, to check this http://www.google.com'
        lang = tweets_analyzer.get_language_from_tweet(tweet)

        self.assertEquals(lang, "en")

        tweet = 'Bonjour, ceci un tweet de test, je mentionne @utilisateur!'
        lang = tweets_analyzer.get_language_from_tweet(tweet)

        self.assertEquals(lang, "fr")


    def test_get_users_in_tweet(self):
        with open(SAMPLE_TIMELINE, encoding='utf-8') as f:
            timeline = json.loads(f.read())
            tweet = timeline[0]

            users = tweets_analyzer.get_users_in_tweet(tweet)
            self.assertEquals(len(users), 4)

    
    def test_get_sentiment_from_timeline(self):
        with open(SAMPLE_TIMELINE, encoding='utf-8') as f:
            timeline = json.loads(f.read())
            sentiment = tweets_analyzer.get_sentiment_from_timeline(timeline)
            self.assertGreater(sentiment, 0)