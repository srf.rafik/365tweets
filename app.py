from PyInquirer import prompt, Separator
from lib import tweets_analyzer

questions = [
    {
        'type': 'rawlist',
        'name': 'mode',
        'message': 'Make your choice',
        'choices': [
            'Top user in a twitter timeline',
            Separator(),
            'Sentiment analysis of user\'s tweets',
            Separator(),
            'Tweet language detection',
        ]
    },
    {
        'type': 'input',
        'name': 'username',
        'message': 'Twitter account ? (without @)'
    }
]

answers = prompt(questions)
username = answers['username']
mode = answers['mode']

try:
    if mode == 'Tweet language detection':
        
        language = tweets_analyzer.get_last_tweet_language(username)
        if language:
            print ('Detected language  for the last of tweet {} : {}'.format(username, language))

    elif mode == 'Sentiment analysis of user\'s tweets':
        sentiment = tweets_analyzer.get_user_sentiments(username)
        if sentiment:
            print ('Rather ', sentiment)  

    elif mode == 'Top user in a twitter timeline':
        top_users = tweets_analyzer.get_top_users(username)
        if top_users:
            print ('Top 5 users in descending order: \n', top_users)
        
    
except Exception as e:
    print('An error has been occured', str(e))

