from twitter import Twitter, OAuth
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from textblob import TextBlob
import sys
import settings
import json
import re
import numpy as np
from heapq import nlargest

def get_user_timeline(username, count=1000):
    twitter = Twitter(auth = OAuth(settings.access_token,
                    settings.access_token_secret,
                    settings.api_key,
                    settings.api_secret_key))

    user_timeline = twitter.statuses.user_timeline(screen_name = username, count=count)
    return user_timeline

def get_last_tweet(username):  
    timeline = get_user_timeline(username, count=1)
    if timeline:
        return timeline[0] 


#remove links and mentions from a tweet text
def clean_tweet (tweet):
    tweet = re.sub(r"@\S+", "", tweet)  # user menstions
    tweet = re.sub(r"http\S+", "", tweet) # links
    return tweet

def get_sentiment_from_timeline(timeline):
    sentiments = []
    analyser = SentimentIntensityAnalyzer()
    for tweet in timeline:
        if tweet['lang'] != 'en':
            continue
                
        if tweet['retweeted'] == True:
            continue

        text = clean_tweet(tweet['text'])
        sentiment = analyser.polarity_scores(text)
        sentiments.append(sentiment['compound'])

    return np.mean(sentiments)


def get_language_from_tweet(tweet):
    clean = clean_tweet(tweet)
    b = TextBlob(clean)
    return b.detect_language()


# get the most present users in a given tweet. 
def get_users_in_tweet(tweet):

   users = []
   users.append(tweet['user']['name'])

   if 'retweeted_status' in tweet:
       u = tweet['retweeted_status']['user']['name'] 
       users.append(u)

   if 'quoted_status' in tweet:
       u = tweet['quoted_status']['user']['name']
       users.append(u) 

   extended_tweet = tweet
   if 'extended_tweet' in tweet:
       extended_tweet = tweet['extended_tweet']

   mentions = extended_tweet['entities']['user_mentions']
   for mention in mentions:
       u = mention['name'] 
       if u not in users: 
           users.append(u) 
   
   return users


def get_top_users_from_timeline(timeline):

    users_stats = {}

    for tweet in timeline:
        users = get_users_in_tweet(tweet)
        for u in users:
            if u in users_stats:
                users_stats[u] += 1
            else:
                users_stats[u] = 1

    return nlargest(5, users_stats, key=users_stats.get)


def get_top_users(username):
    
    try:
        timeline = get_user_timeline(username)
        if timeline: 
           return get_top_users_from_timeline(timeline)
        else:
            print ('Could not get user timeline for ', username)  

    except Exception as e:
        print(str(e))


def get_user_sentiments(username):
    try:
        timeline = get_user_timeline(username)
        if timeline:
            sentiment = get_sentiment_from_timeline(timeline)
            if sentiment > 0:
                return 'Positive'
            else:
                return 'Negative'
        else:
            print ('Could not get user timeline for ', username)  
    
    except Exception as e:
        print (str(e))


# detect the language of thast tweet of a given user 
def get_last_tweet_language(username):
    try:
        tweet = get_last_tweet(username)
        if tweet and 'text' in tweet:
            return get_language_from_tweet(tweet['text'])
        else:
            print('Could not get the last tweet of', username)
    
    except Exception as e:
        print(str(e))